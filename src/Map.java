import javafx.scene.canvas.GraphicsContext;

public class Map
{
    public static final int WIDTH = 12;
    public static final int HEIGHT = 9;

    private Tile[][] map;

    public Map()
    {
        map = new Tile[WIDTH][HEIGHT];
        setMap();
    }

    private void setMap()
    {
        for (int x = 0; x < WIDTH; x++)
        {
            for (int y = 0; y < HEIGHT; y++)
            {
                int xCord = (x * Tile.DIMENSION) + (Tile.DIMENSION / 2);
                int yCord = (y * Tile.DIMENSION) + (Tile.DIMENSION / 2);
                map[x][y] = new Tile(new Vector2d(xCord, yCord), Tile.MAX_LVL_OF_GRASS, x, y);
            }
        }
    }

    public void update()
    {
        for (int x = 0; x < WIDTH; x++)
        {
            for (int y = 0; y < HEIGHT; y++)
            {
                map[x][y].update();
            }
        }
    }

    public void render(GraphicsContext graphic)
    {
        int dupa = 1;
        for (int x = 0; x < WIDTH; x++)
        {
            for (int y = 0; y < HEIGHT; y++)
            {
                map[x][y].draw(graphic);
                dupa = 0;
            }
        }
    }

    public Tile getTile(int _x, int _y)
    {
        return map[_x][_y];
    }
}
