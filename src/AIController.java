import javafx.scene.canvas.GraphicsContext;
import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.abs;

public class AIController
{
    private ArrayList<Snail> snailList;
    private Map map;

    public AIController(Map map)
    {
        this.map = map;
        snailList = new ArrayList<>();
    }

    public void addAI(int amount)
    {
        int tile_x = 0;
        int tile_y = 0;

        Random generator = new Random();

        for (int i = 0; i < amount; i++)
        {
            tile_x = abs(generator.nextInt() % Map.WIDTH);
            tile_y = abs(generator.nextInt() % Map.HEIGHT);

            Snail snail = new Snail(map.getTile(tile_x, tile_y), map.getTile(tile_x, tile_y), map);
            snailList.add(snail);
            snail.setDaemon(true);
            snail.start();
        }
    }

    public void update()
    {
        for (Snail snail : snailList)
        {
            snail.update();
        }
    }

    public void render(GraphicsContext graphic)
    {
        for (Snail snail : snailList)
        {
            snail.draw(graphic);
        }
    }
}
