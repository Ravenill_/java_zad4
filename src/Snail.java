import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.Random;

public class Snail extends Thread
{
    private Vector2d position;

    private Tile posActual;
    private Tile posDest;

    private Map mapReference;

    public static int SNAIL_SIZE = 24;

    public Snail(Tile _actual_tile, Map map)
    {
        posActual = _actual_tile;
        posDest = new Tile();
        position = new Vector2d(posActual.getCords().x, posActual.getCords().y);
        mapReference = map;
    }

    public Snail(Tile _actual_tile, Tile _dest_tile, Map map)
    {
        posActual = _actual_tile;
        posDest = _dest_tile;
        position = new Vector2d(posActual.getCords().x, posActual.getCords().y);
        mapReference = map;
    }

    @Override
    public void run()
    {
        while (true)
        {
            update();
            try
            {
                sleep(10);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void setDest(Tile _dest_tile)
    {
        posDest = _dest_tile;
    }

    public void draw(GraphicsContext graphic)
    {
        Image spirit = new Image("snail.png");
        graphic.drawImage(spirit, position.x - SNAIL_SIZE / 2, position.y - SNAIL_SIZE / 2);
        //graphic.setFill(Color.color(0, 0, 0));
        //graphic.fillRect(position.x - SNAIL_SIZE / 2, position.y - SNAIL_SIZE / 2, SNAIL_SIZE, SNAIL_SIZE);
    }

    public void update()
    {
        //if (!((posActual.no_x >= posDest.no_x - 5) && (posActual.no_x <= posDest.no_x + 5)) && !((posActual.no_y >= posDest.no_y - 5) && (posActual.no_y <= posDest.no_y + 5)))
        if (!((posActual.no_x == posDest.no_x) && (posActual.no_y == posDest.no_y)))
        {
            move();
            if ((posDest.getCords().x == Math.round(position.x)) && (posDest.getCords().y == Math.round(position.y)))
                posActual = posDest;
        }
        else
        {
            posActual.setOccupied(true);
            if (posActual.getLvlOfGrass() < 1)
            {
                posActual.setOccupied(false);
                posDest = findNewTarget();
            }
        }
    }

    private Tile findNewTarget()
    {
        Random generator = new Random();
        Tile result = null;

        do
        {
            int newDestX = -1;
            while (!(newDestX > 0 && newDestX < Map.WIDTH))
                newDestX = posActual.no_x + ((generator.nextInt() % 3));
            int newDestY = -1;
            while (!(newDestY > 0 && newDestY < Map.HEIGHT))
                newDestY = posActual.no_y + ((generator.nextInt() % 3));

            result = mapReference.getTile(newDestX, newDestY);
        }while (result.isOccupied() != false);

        return result;
    }

    private void move()
    {
        moveX();
        moveY();
    }

    private void moveX()
    {
        if (position.x < posDest.getCords().x)
        {
            position.x += 0.5;
        }
        else
        {
            position.x -= 0.5;
        }
    }

    private void moveY()
    {
        if (position.y < posDest.getCords().y)
        {
            position.y += 0.5;
        }
        else
        {
            position.y -= 0.5;
        }
    }
}
