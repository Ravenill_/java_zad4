import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Tile
{
    private Vector2d cords;
    private double lvlOfGrass;

    private boolean isOccupied;
    public int no_x;
    public int no_y;

    public static final int DIMENSION = 50;
    public static final int MAX_LVL_OF_GRASS = 100;

    public Tile()
    {
        this.cords = new Vector2d(0,0);
        this.lvlOfGrass = MAX_LVL_OF_GRASS;
        this.isOccupied = false;
        no_x = 0;
        no_y = 0;
    }

    public Tile(Vector2d cords, int lvlOfGrass, int _x, int _y)
    {
        this.cords = cords;
        this.lvlOfGrass = lvlOfGrass;
        this.isOccupied = false;
        no_x = _x;
        no_y = _y;
    }

    public boolean isOccupied()
    {
        return isOccupied;
    }

    public Vector2d getCords()
    {
        return cords;
    }

    public void setCords(Vector2d cords)
    {
        this.cords = cords;
    }

    public double getLvlOfGrass()
    {
        return lvlOfGrass;
    }

    public void increaseLvlOfGrass()
    {
        if (lvlOfGrass < 100)
            lvlOfGrass += 0.2;
    }

    public void decreaseLvlOfGrass()
    {
        if (lvlOfGrass > 0)
            lvlOfGrass -= 2;
    }

    public void setOccupied(boolean _set)
    {
        isOccupied = _set;
    }

    public void update()
    {
        if (isOccupied)
        {
            decreaseLvlOfGrass();
        }
        else
        {
            increaseLvlOfGrass();
        }
    }
    public void draw(GraphicsContext graphic)
    {
        if (lvlOfGrass < 1)
            graphic.setFill(Color.color(0.46, 0.52, 0.45));
        else if (lvlOfGrass < 25)
            graphic.setFill(Color.color(0.31, 0.42, 0.27));
        else if (lvlOfGrass < 50)
            graphic.setFill(Color.color(0.25, 0.42, 0.11));
        else if (lvlOfGrass < 75)
            graphic.setFill(Color.color(0.15, 0.42, 0));
        else
            graphic.setFill(Color.color(0.13, 0.32, 0));

        graphic.fillRect(cords.x - Tile.DIMENSION / 2, cords.y - Tile.DIMENSION / 2, Tile.DIMENSION, Tile.DIMENSION);

    }
}
