import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class Game extends Application
{
    private Map map;
    private AIController aiController;

    public Game()
    {
        map = new Map();
        aiController = new AIController(map);
        aiController.addAI(3);
    }

    @Override
    public void start(Stage stage) throws Exception
    {
        stage.setTitle("Snail Simulator");

        Group root = new Group();
        Scene scene = new Scene(root);
        stage.setScene(scene);

        Canvas canvas = new Canvas(Tile.DIMENSION * Map.WIDTH, Tile.DIMENSION * Map.HEIGHT);
        root.getChildren().add(canvas);

        GraphicsContext graphic = canvas.getGraphicsContext2D();

        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                update();
                render(graphic);
            }
        }.start();

        stage.show();
    }

    public void launchSim(String[] args)
    {
        launch(args);
    }

    private void update()
    {
        //aiController.update();
        map.update();
    }

    private void render(GraphicsContext graphic)
    {
        //graphic.clearRect(0, 0, Map.WIDTH * Tile.DIMENSION,Map.HEIGHT * Tile.DIMENSION);
        map.render(graphic);
        aiController.render(graphic);
    }
}
